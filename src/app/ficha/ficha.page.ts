import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.page.html',
  styleUrls: ['./ficha.page.scss'],
})
export class FichaPage implements OnInit {
  hijo = {id:0, nombre: '',nacionalidad:'', lugar_nacimiento: '', img:''}
  id = '';
  publicaciones = 0;
  seguidores = 0;
  edad = 0;
  sexo = '';
  fecha: Date;
  apiurl = '';

  constructor(private libro: LibroService,
    private activatedRoute: ActivatedRoute) {
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.gethijo();
    this.getseguidores();
    this.getsonpost(this.id);
  }

  gethijo(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        if(Number(this.id) == element.id)
        {
          this.hijo = {id: element.id, nombre: element.nombre + ' ' + element.apellido_paterno + ' ' + element.apellido_materno, nacionalidad: element.nacionalidad, lugar_nacimiento: element.lugar_nacimiento, img: element.foto}
          this.fecha = element.fecha_nacimiento
          console.log('hijo:', this.hijo)
          this.calcularedad(element.fecha_nacimiento)
          this.setgenero(element.genero)
        }
      });
    }, error =>{
      console.log(error)
    } ) 
  }

  getseguidores(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      this.seguidores = result.data[0].followers
    }, error =>{
      console.log(error)
    })
  }

  getsonpost(id: string){
    this.libro.getpostson(Number(id)).subscribe((result:any) => {
      console.log(result);
      this.publicaciones = result[0].data.length
    },error => {
      console.log(error)
    })
  }

  calcularedad(fecha: Date){
    if (fecha) {
      var timeDiff = Math.abs(Date.now() - new Date(fecha).getTime());
      this.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
  }

  setgenero(id: number){
    if(id == 1)
    {
      this.sexo = 'Hombre'
    }else{
      if(id == 2)
      {
        this.sexo = 'Mujer'
      }
      else{
        this.sexo = 'Otro'
      }
    }

  }

}
