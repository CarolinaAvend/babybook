import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-muro',
  templateUrl: './muro.page.html',
  styleUrls: ['./muro.page.scss'],
})
export class MuroPage implements OnInit {
  usuario = {id: 0, nombre: '', apellido: '', nacionalidad: '', img: '/assets/img/ico-user-72x72.svg'}
  imagenes = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
  apiurl = '';
  test = ''
  type = '';
  id = '';
  hijos = [];
  edad = 0;
  posts = []

  constructor(private libro: LibroService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
    this.type = this.activatedRoute.snapshot.paramMap.get('tipo')
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    console.log(this.type)
    console.log(this.id)
  }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.hijos = []
    this.gethijos()
    if(this.type == 'hijo')
    {
      this.getsonpost(this.id)
    }else{
      this.getallpost()
    } 
  }

  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        if(this.type == 'hijo')
        {
          if (element.id == this.id)
          {
            this.usuario = {id: element.id, nombre: element.nombre,  apellido:element.apellido_paterno, nacionalidad: element.nacionalidad, img: element.foto}
            this.calcularedad(element.fecha_nacimiento)
          }else{
            this.hijos.push({id: element.id, nombre:element.nombre, foto: element.foto})
          }
        }
        else{
          this.hijos.push({id: element.id, nombre:element.nombre, foto: element.foto})
        }
      });
    }, error =>{
      console.log(error)
    } )      
  }

  calcularedad(fecha: Date){
    if (fecha) {
      var timeDiff = Math.abs(Date.now() - new Date(fecha).getTime());
      this.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
    }
  }

  getallpost(){
    this.libro.getpost().subscribe((result:any) => {
      console.log(result);
      this.posts = result[0].data
      console.log('post',this.posts)
    }, error => {
      console.log(error);
    })
  }

  getsonpost(id: string){
    this.libro.getpostson(Number(id)).subscribe((result:any) => {
      console.log(result);
      this.posts = result[0].data
      this.test = result[0].data[3].pictures[0].picture
      console.log('post',this.posts)
    },error => {
      console.log(error)
    })
  }

  gopost(id: number){
    let infopost = []
    this.posts.forEach(element => {
      if (element.id == id)
      {
        infopost = element
      }
    })
    console.log(infopost)
    let navigationExtras: NavigationExtras = { state: { post: infopost } };
    this.router.navigate(['/tabs/post/',id], navigationExtras)  
  }

  

}
