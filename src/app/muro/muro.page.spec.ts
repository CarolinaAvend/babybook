import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MuroPage } from './muro.page';

describe('MuroPage', () => {
  let component: MuroPage;
  let fixture: ComponentFixture<MuroPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MuroPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MuroPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
