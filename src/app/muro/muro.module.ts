import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { IonicModule } from '@ionic/angular';

import { MuroPageRoutingModule } from './muro-routing.module';

import { MuroPage } from './muro.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MuroPageRoutingModule,
    FontAwesomeModule
  ],
  declarations: [MuroPage]
})
export class MuroPageModule {}
