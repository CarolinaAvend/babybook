import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-tab4',
  templateUrl: './tab4.page.html',
  styleUrls: ['./tab4.page.scss'],
})
export class Tab4Page implements OnInit {

notificaciones = [
    {id:1, nombre: 'Nombre', apellido:'Seguidor', foto: '/assets/img/ico-user-72x72.svg', texto:'Comento tu publicación', img: true, imgurl:'https://via.placeholder.com/40x40', chip: false, chiptext:''},
    {id:2, nombre: 'Nombre', apellido:'Seguidor', foto: '/assets/img/ico-user-72x72.svg', texto:'Le gusta tu publicación', img: true, imgurl:'https://via.placeholder.com/40x40', chip: false, chiptext:''},
    {id:3, nombre: 'Nombre', apellido:'Seguidor', foto: '/assets/img/ico-user-72x72.svg', texto:'Solicito seguir a Nombre Babybook', img: false, imgurl:'https://via.placeholder.com/40x40', chip: true, chiptext:'Aceptar'},
    {id:4, nombre: 'Nombre', apellido:'Seguidor', foto: '/assets/img/ico-user-72x72.svg', texto:'Etiqueto a Nombre Babybook en una publicación', img: true, imgurl:'https://via.placeholder.com/40x40', chip: false, chiptext:'Aceptar'},
    {id:5, nombre: 'Nombre', apellido:'Seguidor', foto: '/assets/img/ico-user-72x72.svg', texto:'Le gusta tu comentario', img: true, imgurl:'https://via.placeholder.com/40x40', chip: false, chiptext:'Aceptar'},
    {id:6, nombre: 'Babybook', apellido:'', foto: '/assets/img/IMG_2096.PNG', texto:'Te recuerda actualizar el libro de Nombre Babybook', img: false, imgurl:'https://via.placeholder.com/40x40', chip: true, chiptext:'Actualizar'},
]

  constructor(private libro: LibroService) { }

  ngOnInit() {
  }

}
