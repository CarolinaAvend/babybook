import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-publicar',
  templateUrl: './publicar.page.html',
  styleUrls: ['./publicar.page.scss'],
})
export class PublicarPage implements OnInit {
  type = '';
  privado = true;
  publico = false;
  facebook = false;
  instagram = false;
  placehol = '';
  hijos: any;
  texto: any;
  nombres = '';
  countname = 0;
  fotos: any;
  video: any;
  extra = new FormControl('',Validators.required);
  titulo = '';
  aux = '';
  isKeyboardHide=true;

  slideOpts = {
    initialSlide: 0,
    speed: 400
};  


  constructor(private activatedRoute: ActivatedRoute,
    private libro: LibroService,
    private router: Router,
    public keyboard:Keyboard,
    public alertController: AlertController,) {
    this.type = this.activatedRoute.snapshot.paramMap.get('tipo')
    if (this.type == 'texto'){
      this.placehol = "Agrega un título a tu publicación...";
    }else{
      this.placehol = "Escribe aquí el texto de tu publicación...";
    }
    this.activatedRoute.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.hijos = this.router.getCurrentNavigation().extras.state.hijos;
        this.texto = this.router.getCurrentNavigation().extras.state.texto;
        this.fotos = this.router.getCurrentNavigation().extras.state.foto;
        this.video = this.router.getCurrentNavigation().extras.state.video;
      }
    });
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.nombres = ''
    this.countname = 0
    this.aux = ''
    this.setnombres();
    console.log(this.texto)
    this.keyboard.onKeyboardWillShow().subscribe(()=>{
      this.isKeyboardHide=false;
       console.log('SHOWK');
    });

    this.keyboard.onKeyboardWillHide().subscribe(()=>{
      this.isKeyboardHide=true;
      console.log('HIDEK');
    });
  }

  ToogleChange(index:number,valor: boolean) {
    if (index == 1){
      this.publico = !valor;
    }
    if (index == 2){
      this.privado = !valor;
    }
  }

  setnombres(){
    this.hijos.forEach(element => {
      if (element.seleccionado)
      {
        if (this.countname > 0){
          this.nombres = this.nombres + ', ' + element.nombre
          this.aux = this.aux + ', {"id":' + element.id + '}' 
        }else{
          this.nombres =  element.nombre
          this.aux =  '{"id":' + element.id + '}'
        }
        this.countname = this.countname + 1
      }
    });
    console.log(this.aux)
  }


  addpost(){
    let privacidad = 0;
    if (this.privado){
      privacidad = 1;
    }
    if (this.publico){
      privacidad = 2;
    }
    if (this.type == 'texto')
    {
      this.titulo = this.extra.value
    }else{
      this.texto = this.extra.value
    }
    //let sons = '[{"id":1}, {"id": 2}]';
    let sons = '[' + this.aux + ']';
    this.libro.addpost(this.titulo,this.texto,privacidad,sons).subscribe((data: any)=>{
      console.log(data)
      let post_id = data.insertId
      if (this.fotos.lenght > 0){
        console.log('fotos!!')
        this.fotos.forEach(foto => {
          if (foto && foto != '' || foto != null){
            this.libro.addUpload('',post_id,foto).subscribe((result:any) => {
              console.log(result)
            }, error => {
              console.log(error)
            })
          }
        });

        
      }
      
      this.router.navigate([''])
    }, error =>{
      console.log(error)
    } )      
  }

  deletePhoto(index) {
    this.confirm(index)
  }


  async confirm(index) {
    const alert = await this.alertController.create({
      message: '¿Eliminar foto?',
      cssClass: 'alert-css',
      buttons: [
        {
          text: 'No',
          handler: () => {
            console.log('Disagree clicked');
          }
        }, {
          text: 'Si',
          handler: () => {
            console.log('Agree clicked');
            this.fotos.splice(index, 1);
          }
        }
      ]
    });

    await alert.present();
  }
}
