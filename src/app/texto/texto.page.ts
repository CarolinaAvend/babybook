import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { FormControl, Validators } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-texto',
  templateUrl: './texto.page.html',
  styleUrls: ['./texto.page.scss'],
})
export class TextoPage implements OnInit {
  hijos: any;
  nombres = '';
  countname = 0;
  publicacion = new FormControl('',Validators.required);
  isKeyboardHide=true;


  constructor(private libro: LibroService,
    private route: ActivatedRoute, 
    private router: Router,
    public keyboard:Keyboard) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.hijos = this.router.getCurrentNavigation().extras.state.hijos;
        }
      });
    }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.nombres = ''
    this.countname = 0
    console.log(this.hijos)
    this.setnombres();
    this.keyboard.onKeyboardWillShow().subscribe(()=>{
      this.isKeyboardHide=false;
       console.log('SHOWK');
    });

    this.keyboard.onKeyboardWillHide().subscribe(()=>{
      this.isKeyboardHide=true;
      console.log('HIDEK');
    });
  }

  setnombres(){
    this.hijos.forEach(element => {
      if (element.seleccionado)
      {
        if (this.countname > 0){
          this.nombres = this.nombres + ', ' + element.nombre
        }else{
          this.nombres =  element.nombre
        }
        this.countname = this.countname + 1
      }
    });
  }

  gofoto(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/foto'], navigationExtras)
  }

  govideo(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/video'], navigationExtras)
  }

  gopublicar(){
    if (!this.publicacion.errors){
      let navigationExtras: NavigationExtras = { state: { hijos: this.hijos, texto: this.publicacion.value } };
      this.router.navigate(['/publicar/texto'], navigationExtras)
    }
  }

}
