import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SeguidoresPage } from './seguidores.page';

describe('SeguidoresPage', () => {
  let component: SeguidoresPage;
  let fixture: ComponentFixture<SeguidoresPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SeguidoresPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SeguidoresPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
