import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-opciones',
  templateUrl: './opciones.page.html',
  styleUrls: ['./opciones.page.scss'],
})
export class OpcionesPage implements OnInit {

  constructor(public modalCtrl: ModalController,
    private libro: LibroService) { }

  ngOnInit() {
  }

  dismiss() {
    this.modalCtrl.dismiss();
  }

}
