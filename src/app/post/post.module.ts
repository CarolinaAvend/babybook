import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OpcionesPageModule } from '../opciones/opciones.module';
import { ComentariosPageModule } from '../comentarios/comentarios.module';
import { DatePipe } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { PostPageRoutingModule } from './post-routing.module';

import { PostPage } from './post.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostPageRoutingModule,
    FontAwesomeModule,
    OpcionesPageModule,
    ComentariosPageModule
  ],
  declarations: [PostPage],
  providers: [DatePipe]
})
export class PostPageModule {}
