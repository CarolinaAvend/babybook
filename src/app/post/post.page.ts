import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OpcionesPage } from '../opciones/opciones.page';
import { ComentariosPage } from '../comentarios/comentarios.page';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-post',
  templateUrl: './post.page.html',
  styleUrls: ['./post.page.scss'],
})
export class PostPage implements OnInit {
post: any;
usuario = {
  id:0,
  username:'',
  img: '',
}
apiurl = '';
time = 'Hace 2 días';
user = '';
img = '';
likes = false;
aux = [];
userid = 0;

slideOpts = {
    initialSlide: 0,
    speed: 400
};  

  constructor(public modalController: ModalController,
    private libro: LibroService,
    private activatedRoute: ActivatedRoute,
    private router: Router) {
      this.activatedRoute.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.post = this.router.getCurrentNavigation().extras.state.post;
        }
      });
    }

  ngOnInit() {
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.getuser()
  }

  getuser(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      this.usuario = {id:result.data[0].id, username:result.data[0].name + ' ' + result.data[0].lastname, img: result.data[0].picture}
      this.userid = result.data[0].id
      this.user = result.data[0].name + ' ' + result.data[0].lastname
      this.img = result.data[0].picture
      console.log(this.usuario)
      this.megusta()
    }, error =>{
      console.log(error)
    })
    console.log(this.usuario)
  }

  async openopcion() {
    const modal = await this.modalController.create({
    component: OpcionesPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
    });
  
    return await modal.present();
   }

   async opencomentarios() {
    let text
    if (this.post.pictures && this.post.pictures.length > 0){
      text = this.post.description
    }else{
      text = this.post.title
    }
    const modal = await this.modalController.create({
    component: ComentariosPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true,
    componentProps: { 
      comentarios: this.post.comments,
      usuario: this.user,
      id: this.post.id,
      descripcion: text,
      imagen: this.img,
      postfecha: this.post.fecha,
      totalcomentario:this.post.comments_count,
      apiurl: this.apiurl
    }
    });
  
    modal.onDidDismiss()
    .then((data) => {
      console.log("data:", data)
    });
  
    return await modal.present();
   } 

   like(id: any){
    if (!this.likes){
      this.libro.addlike(id).subscribe((data: any) => {
        console.log(data)
        this.post.likes_count = this.post.likes_count + 1
        this.likes = true
      }, error => {
        console.log(error)
      })
    }
  }

  megusta(){
    this.aux = this.post.likes
    this.aux.forEach(element => {
      if (element.author_id == this.userid){
        this.likes = true
      }
    })
  }

}
