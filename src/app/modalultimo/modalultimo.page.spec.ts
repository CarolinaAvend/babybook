import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalultimoPage } from './modalultimo.page';

describe('ModalultimoPage', () => {
  let component: ModalultimoPage;
  let fixture: ComponentFixture<ModalultimoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalultimoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalultimoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
