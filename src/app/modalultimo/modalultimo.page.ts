import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-modalultimo',
  templateUrl: './modalultimo.page.html',
  styleUrls: ['./modalultimo.page.scss'],
})
export class ModalultimoPage implements OnInit {
  terminos = false;

  constructor(public modalCtrl: ModalController,
    private router: Router) { }

  ngOnInit() {
  }

  dismiss(terminos: any) {
    this.modalCtrl.dismiss(terminos);

  }

}
