import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalultimoPageRoutingModule } from './modalultimo-routing.module';

import { ModalultimoPage } from './modalultimo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalultimoPageRoutingModule,
  ],
  declarations: [ModalultimoPage]
})
export class ModalultimoPageModule {}
