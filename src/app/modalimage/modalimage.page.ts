import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LibroService } from 'src/app/service/libro.service';
import { Camera } from '@ionic-native/camera/ngx';

@Component({
  selector: 'app-modalimage',
  templateUrl: './modalimage.page.html',
  styleUrls: ['./modalimage.page.scss'],
})
export class ModalimagePage implements OnInit {
  image_url = ''

  constructor(public modalCtrl: ModalController,
    private libro: LibroService,
    private camera: Camera) { }

  ngOnInit() {
  }

  dismiss(id:any) {
    this.modalCtrl.dismiss(id);
  }
}
