import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { IonicModule } from '@ionic/angular';

import { BabybookPageRoutingModule } from './babybook-routing.module';

import { BabybookPage } from './babybook.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BabybookPageRoutingModule,
    FontAwesomeModule
  ],
  declarations: [BabybookPage]
})
export class BabybookPageModule {}
