import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { BabybookPage } from './babybook.page';

describe('BabybookPage', () => {
  let component: BabybookPage;
  let fixture: ComponentFixture<BabybookPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BabybookPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(BabybookPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
