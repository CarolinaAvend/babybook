import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-libro',
  templateUrl: './libro.page.html',
  styleUrls: ['./libro.page.scss'],
})
export class LibroPage implements OnInit {
  usuario = {
    id:0, 
    username:'',
  }
  hijos = [];

  constructor(private libro: LibroService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.usuario = {
      id:0, 
      username:'',
    }
    this.hijos = [];
    this.getuser();
    this.gethijos();
  }

  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        this.hijos.push({id: element.id, nombre:element.nombre, current: 0})
      });
      console.log(this.hijos)
    }, error =>{
      console.log(error)
    } )      
  }

  getuser(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      this.usuario = {id:result.data[0].id, username:result.data[0].username}
      console.log(this.usuario)
    }, error =>{
      console.log(error)
    })
    console.log(this.usuario)
  }

}
