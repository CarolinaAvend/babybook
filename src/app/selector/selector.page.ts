import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-selector',
  templateUrl: './selector.page.html',
  styleUrls: ['./selector.page.scss'],
})
export class SelectorPage implements OnInit {
  hijos = []
  test = []
  count = 0;
  apiurl = ''

  constructor(public modalCtrl: ModalController,
    private router: Router,
    private libro: LibroService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.hijos = [];
    this.gethijos();
  }

  select(index: number){
    if (this.hijos[index].seleccionado){
      this.hijos[index].seleccionado = false
      this.count = this.count - 1 
    }
    else{
      this.hijos[index].seleccionado = true
      this.count = this.count + 1
    }
    this.test = this.hijos
  }

  dismiss() {
    let navigationExtras: NavigationExtras = { state: { hijos: this.test } };
    this.modalCtrl.dismiss();
    if(this.count > 0){
      this.count = 0;
      this.router.navigate(['/texto'], navigationExtras)
    }
    //this.selectoff();
  }
  
  selectoff(){
    console.log('off')
    for(let i=0;i<this.hijos.length;i++){
      this.hijos[i].seleccionado = false
    }
  }

  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        this.hijos.push({id: element.id, nombre:element.nombre, foto: element.foto, seleccionado: false})
      });
    }, error =>{
      console.log(error)
    } )      
  }

}
