import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'texto',
    loadChildren: () => import('./texto/texto.module').then( m => m.TextoPageModule)
  },
  {
    path: 'selector',
    loadChildren: () => import('./selector/selector.module').then( m => m.SelectorPageModule)
  },
  {
    path: 'feed',
    loadChildren: () => import('./feed/feed.module').then( m => m.FeedPageModule)
  },
  {
    path: 'publicar/:tipo',
    loadChildren: () => import('./publicar/publicar.module').then( m => m.PublicarPageModule)
  },
  {
    path: 'foto',
    loadChildren: () => import('./foto/foto.module').then( m => m.FotoPageModule)
  },
  {
    path: 'video',
    loadChildren: () => import('./video/video.module').then( m => m.VideoPageModule)
  },
  {
    path: 'comentarios',
    loadChildren: () => import('./comentarios/comentarios.module').then( m => m.ComentariosPageModule)
  },
  {
    path: 'registro',
    loadChildren: () => import('./registro/registro.module').then( m => m.RegistroPageModule)
  },
  {
    path: 'opciones',
    loadChildren: () => import('./opciones/opciones.module').then( m => m.OpcionesPageModule)
  },
  {
    path: 'modalultimo',
    loadChildren: () => import('./modalultimo/modalultimo.module').then( m => m.ModalultimoPageModule)
  },
  {
    path: 'modalopcion',
    loadChildren: () => import('./modalopcion/modalopcion.module').then( m => m.ModalopcionPageModule)
  },
  {
    path: 'modalnacionalidades',
    loadChildren: () => import('./modalnacionalidades/modalnacionalidades.module').then( m => m.ModalnacionalidadesPageModule)
  },
  {
    path: 'modalimage',
    loadChildren: () => import('./modalimage/modalimage.module').then( m => m.ModalimagePageModule)
  },
  {
    path: 'preview',
    loadChildren: () => import('./preview/preview.module').then( m => m.PreviewPageModule)
  },
  {
    path: 'imagenop',
    loadChildren: () => import('./imagenop/imagenop.module').then( m => m.ImagenopPageModule)
  },






];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
