import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LibroService } from 'src/app/service/libro.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalopcionPage } from '../modalopcion/modalopcion.page';
import { ModalnacionalidadesPage } from '../modalnacionalidades/modalnacionalidades.page';
import { ModalController } from '@ionic/angular';
import { ModalimagePage } from '../modalimage/modalimage.page';
import { Camera } from '@ionic-native/camera/ngx';
import { Crop,CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.page.html',
  styleUrls: ['./agregar.page.scss'],
})
export class AgregarPage implements OnInit {
  agregarform: FormGroup;
  generoselect = '';
  nacionalidadselect = '';
  masdatos = false;
  image = ''
  selectedDate;
  disabledDates: Date[] = [];
  datePickerObj: any = {
    inputDate: new Date(), 
    fromDate: new Date('1950-01-01'),
    toDate: new Date('2100-01-01'), 
    closeOnSelect: true,
    mondayFirst: true, 
    todayLabel: 'Hoy', 
    closeLabel: 'Cerrar', 
    titleLabel: 'Seleccionar Fecha',
    monthsList: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    weeksList: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
    dateFormat: 'YYYY-MM-DD', 
    clearButton : false , 
    momentLocale: 'es-CL', 
    yearInAscending: true, 
    btnCloseSetInReverse: true, 
    btnProperties: {
      expand: 'block', 
      fill: '', 
      size: '', 
      disabled: '', 
      strong: '', 
      color: ''
    },
    arrowNextPrev: {}, 
    highlightedDates: [],
    isSundayHighlighted : {
     fontColor: '#ee88bf' } 
  };

  cropOptions: CropOptions = {
    quality: 50
  }

  constructor(
    public formbuilder: FormBuilder,
    public alertController: AlertController,
    private libro: LibroService,
    private router: Router,
    public modalController: ModalController,
    private loadingController: LoadingController,
    private camera: Camera,
    private crop: Crop,
    private file: File
  ) {
    this.agregarform = this.formbuilder.group({
      nombre: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      apellido: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      materno: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      sexo: new FormControl('',Validators.required),
      fecha: new FormControl('',Validators.required),
      peso: new FormControl(''),
      talla: new FormControl(''),
      apgar: new FormControl(''),
      lugar: new FormControl(''),
      nacionalidad: new FormControl(''),
      pelo: new FormControl(''),
      ojos: new FormControl(''),
      parecido: new FormControl(''),
      direccion: new FormControl(''),
      foto: new FormControl(''),
    })
  }

  ngOnInit() {
  }

  ionViewWillLeave(){
    this.agregarform.controls['nombre'].setValue('');
    this.agregarform.controls['apellido'].setValue('');
    this.agregarform.controls['materno'].setValue('');
    this.agregarform.controls['sexo'].setValue('');
    this.agregarform.controls['fecha'].setValue('');
    this.agregarform.controls['peso'].setValue('');
    this.agregarform.controls['talla'].setValue('');
    this.agregarform.controls['apgar'].setValue('');
    this.agregarform.controls['lugar'].setValue('');
    this.agregarform.controls['nacionalidad'].setValue('');
    this.agregarform.controls['pelo'].setValue('');
    this.agregarform.controls['ojos'].setValue('');
    this.agregarform.controls['parecido'].setValue('');
    this.agregarform.controls['direccion'].setValue('');
    this.agregarform.controls['foto'].setValue('');
    this.image = '';
    this.generoselect = '';
    this.nacionalidadselect = '';
    this.masdatos = false;
  }

  mostrarmasdatos(){
    this.masdatos = true;
  }

  onagregarhijo(){
    let texto = '';
    this.agregarform.controls['foto'].setValue(this.image);
    console.log(this.agregarform.value)
    if (!this.agregarform.valid){
      return false;
    }else{
      this.libro.agregarhijo(this.agregarform).subscribe((data: any)=>{
        console.log("Datos enviados")
        console.log(data)
        texto = data.message;
        this.Alerta(texto,true)
      }, error =>{
        console.log(error)
        texto = error.error.message;
        this.Alerta(texto,false)
      } )      
    }
  }


  async Alerta(text: string, valid: boolean) {
    const alert = await this.alertController.create({
      message: text,
      cssClass: 'alert-css',
      buttons: [
          {
            text: 'OK',
            handler: () => {
              if(valid){
                this.Loading();
              }
            }
          }, 
      ]
    });

    await alert.present();
  }

  async Loading() {
    const loading = await this.loadingController.create({
      duration: 2000,
      message: 'Cargando...',
      cssClass: 'custom-class custom-loading',
    });
    await loading.present();

    const { data } = await loading.onDidDismiss();
    this.router.navigate(['tabs/tab5'])
  }
  
  async openModalOpcion() {
    const modal = await this.modalController.create({
    component: ModalopcionPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.generoselect = modaldata.text;
      this.agregarform.controls['sexo'].setValue(String(modaldata.id));

      console.log(modaldata)
      console.log(this.agregarform.value)
    });
  
    return await modal.present();
   }

   async openModalNacionalidad() {
    const modal = await this.modalController.create({
    component: ModalnacionalidadesPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.nacionalidadselect = modaldata.text;
      this.agregarform.controls['nacionalidad'].setValue(String(modaldata.text));

      console.log(modaldata)
      console.log(this.agregarform.value)
    });
  
    return await modal.present();
   }

   async openModalImage() {
    const modal = await this.modalController.create({
    component: ModalimagePage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      this.onimage(data.data);
    });
  
    return await modal.present();
   }


   onimage(id:any)
   {
     console.log("id", id)
     if (id == 0)
     {
        this.libro.setpicture(this.camera.PictureSourceType.CAMERA).then(data => {
         this.cropImage(data)
       });
     }
     else{
       if (id == 1)
       {
         this.libro.setpicture(this.camera.PictureSourceType.PHOTOLIBRARY).then(data => {
           this.cropImage(data)
         });
       }
       else{
         if (id == 2){
           this.image = '';
         }
       } 
     }
   }

   cropImage(imgPath) {
    this.crop.crop(imgPath, this.cropOptions)
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          console.log(error)
        }
      );
  }

  showCroppedImage(ImagePath) {
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.image = base64;
    }, error => {
      console.log(error)
    });
  }

}
