import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { ModalopcionPageModule } from '../modalopcion/modalopcion.module';
import { ModalnacionalidadesPageModule } from '../modalnacionalidades/modalnacionalidades.module';
import { ModalimagePageModule } from '../modalimage/modalimage.module';

import { IonicModule } from '@ionic/angular';

import { AgregarPageRoutingModule } from './agregar-routing.module';

import { AgregarPage } from './agregar.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgregarPageRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    Ionic4DatepickerModule,
    ModalopcionPageModule,
    ModalimagePageModule,
    ModalnacionalidadesPageModule
  ],
  declarations: [AgregarPage]
})
export class AgregarPageModule {}
