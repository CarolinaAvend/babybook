import { Component } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page {
  usuarios = [];
  searchTerm = '';
  isKeyboardHide=true;
  apiurl = '';

  constructor(private libro: LibroService,
    private loadingController: LoadingController,
    public alertController: AlertController,
    public keyboard:Keyboard) {
    this.usuarios = []
    
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.apiurl = this.libro.geturl()
    this.usuarios = []
    this.keyboard.onKeyboardWillShow().subscribe(()=>{
      this.isKeyboardHide=false;
       console.log('SHOWK');
    });

    this.keyboard.onKeyboardWillHide().subscribe(()=>{
      this.isKeyboardHide=true;
      console.log('HIDEK');
    });
  }




  searchuser(){
    console.log(this.searchTerm)
    this.libro.search(this.searchTerm).subscribe((result: any)=>{
      console.log(result)
      if (result.data.length > 0){
        console.log('if')
        this.usuarios = result.data
       /* result.data.forEach(element => {
          this.usuarios.push({id:element.id, nombre:element.name, apellido:element.lastname, img: '/assets/img/ico-user-72x72.svg', comun:0})
        })*/
      }else{
        this.usuarios = [{id: -1, name: result.message, lastname:''}]
      }
      console.log(this.usuarios)
    }, error =>{
      console.log(error)
    } )      
  }

  seguir(id:any){
    this.libro.addfollow(id).subscribe((result: any)=>{
      console.log(result)
      this.Alerta("Siguiendo!")

    }, error =>{
      console.log(error)
      this.Alerta(error.error.message)
    } )      
  }

  async Alerta(text: string) {
    const alert = await this.alertController.create({
      message: text,
      cssClass: 'alert-css',
      buttons: [
          {
            text: 'OK',
            handler: () => {
            }
          }, 
      ]
    });

    await alert.present();
  }

}
