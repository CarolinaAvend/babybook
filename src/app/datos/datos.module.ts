import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { ModalopcionPageModule } from '../modalopcion/modalopcion.module';
import { ModalnacionalidadesPageModule } from '../modalnacionalidades/modalnacionalidades.module';
import { ModalimagePageModule } from '../modalimage/modalimage.module';
import { ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { DatosPageRoutingModule } from './datos-routing.module';

import { DatosPage } from './datos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DatosPageRoutingModule,
    FontAwesomeModule,
    Ionic4DatepickerModule,
    ModalopcionPageModule,
    ModalnacionalidadesPageModule,
    ModalimagePageModule,
    ReactiveFormsModule
  ],
  declarations: [DatosPage],
  providers: [DatePipe]
})
export class DatosPageModule {}
