import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { ModalopcionPage } from '../modalopcion/modalopcion.page';
import { ModalnacionalidadesPage } from '../modalnacionalidades/modalnacionalidades.page';
import { ModalimagePage } from '../modalimage/modalimage.page';
import { ModalController } from '@ionic/angular';
import { FormControl } from '@angular/forms';
import { AlertController } from '@ionic/angular';
import { formatDate } from '@angular/common';
import { Camera } from '@ionic-native/camera/ngx';
import { Crop,CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-datos',
  templateUrl: './datos.page.html',
  styleUrls: ['./datos.page.scss'],
})
export class DatosPage implements OnInit {
  privada = false;
  generoselect = ' ';
  nacionalidadselect = ' ';
  usuario = {
    picture: '',
    username: '',
    email: '',
    name: '',
    lastname: '',
    gender: 0,
    birth: '',
    nationality: '',
    country: '',
    city: '',
  }
  usuarioaux = {
    picture: '',
    username: '',
    email: '',
    name: '',
    lastname: '',
    gender: 0,
    birth: '',
    nationality: '',
    country: '',
    city: '',
  }
  image: any;

  user = new FormControl('');
  name = new FormControl('');
  apellido = new FormControl('');
  genero = new FormControl('');
  fecha = new FormControl('');
  nacionalidad = new FormControl('');
  pais = new FormControl('');
  ciudad = new FormControl('');
  correo = new FormControl('');

  selectedDate;
  disabledDates: Date[] = [];
  datePickerObj: any = {
    inputDate: new Date(), 
    fromDate: new Date('1950-01-01'),
    toDate: new Date('2100-01-01'), 
    closeOnSelect: true,
    mondayFirst: true, 
    todayLabel: 'Hoy', 
    closeLabel: 'Cerrar', 
    titleLabel: 'Seleccionar Fecha',
    monthsList: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    weeksList: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
    dateFormat: 'YYYY-MM-DD', 
    clearButton : false , 
    momentLocale: 'es-CL', 
    yearInAscending: true, 
    btnCloseSetInReverse: true, 
    btnProperties: {
      expand: 'block', 
      fill: '', 
      size: '', 
      disabled: '', 
      strong: '', 
      color: ''
    },
    arrowNextPrev: {}, 
    highlightedDates: [],
    isSundayHighlighted : {
     fontColor: '#ee88bf' } 
  };

  cropOptions: CropOptions = {
    quality: 50
  }

  apiurl = '';

  constructor(private libro: LibroService,
    public modalController: ModalController,
    public alertController: AlertController,
    private camera: Camera,
    private crop: Crop,
    private file: File) {
    
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.privada = false;
    this.getuser();
  }

  mostrarprivada(){
    this.privada = true;
  }

  getuser(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      if (result.data[0].picture != ''){
        this.image = this.apiurl + result.data[0].picture
      }else{
        this.image = ''
      }
      this.user.setValue(result.data[0].username);
      this.name.setValue(result.data[0].name);
      this.apellido.setValue(result.data[0].lastname);
      this.genero.setValue(result.data[0].gender);
      this.setgenero()
      this.fecha.setValue(formatDate(new Date(result.data[0].birth), 'yyyy-MM-dd','es-CL'));
      this.nacionalidad.setValue(result.data[0].nationality);
      this.nacionalidadselect = result.data[0].nationality;
      this.pais.setValue(result.data[0].country);
      this.ciudad.setValue(result.data[0].city);
      this.correo.setValue(result.data[0].email);
      this.SetUser();
      this.SetUseraux()
      console.log(this.image)
    }, error =>{
      console.log(error)
    })
  }

  SetUser(){
    this.usuario = {
      picture: this.image,
      username: this.user.value,
      email: this.correo.value,
      name: this.name.value,
      lastname: this.apellido.value,
      gender: Number(this.genero.value),
      birth: this.fecha.value,
      nationality: this.nacionalidad.value,
      country: this.pais.value,
      city: this.ciudad.value,
    }
    console.log(this.usuario)
  }

  SetUseraux(){
    this.usuarioaux = {
      picture: this.image,
      username: this.user.value,
      email: this.correo.value,
      name: this.name.value,
      lastname: this.apellido.value,
      gender: Number(this.genero.value),
      birth: this.fecha.value,
      nationality: this.nacionalidad.value,
      country: this.pais.value,
      city: this.ciudad.value,
    }
    console.log(this.usuario)
  }

  setgenero(){
    if (this.genero.value == 1)
    {
      this.generoselect = 'Hombre';
    }
    else {
      if (this.genero.value == 2)
      {
        this.generoselect = 'Mujer';
      }
      else{
        this.generoselect = 'Otro';
      }
    }
  }

  edituser(){
    let texto = ''
    console.log("edituser")
    this.libro.editme(this.usuario).subscribe((data:any) => {
      console.log(data)
      texto = data.message;
      this.Alerta(texto)
      this.SetUser()
      this.SetUseraux()
    }, error =>{
      console.log(error)
      texto = error.error.message;
      this.Alerta(texto)
    })
  }

  haycambios(){
    let cambios = false
    if (this.usuarioaux.picture != this.image && this.usuario.picture != null && this.image != null){cambios = true}
    if (this.usuarioaux.username != this.user.value){cambios = true}
    if (this.usuarioaux.email != this.correo.value){cambios = true}
    if (this.usuarioaux.name != this.name.value){cambios = true}
    if (this.usuarioaux.gender != Number(this.genero.value)){cambios = true}
    if (this.usuarioaux.birth != this.fecha.value && this.usuarioaux.birth != null){cambios = true}
    if (this.usuarioaux.nationality != this.nacionalidad.value){cambios = true}
    if (this.usuarioaux.country != this.pais.value){cambios = true}
    if (this.usuarioaux.city != this.ciudad.value){cambios = true}
    if(cambios){
      this.edituser()
    }
  }

  onblur(){
    console.log("ionblur")
    this.SetUser();
    this.haycambios();
  }

  async openModalOpcion() {
    const modal = await this.modalController.create({
    component: ModalopcionPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.generoselect = modaldata.text;
      this.genero.setValue(String(modaldata.id));
      this.onblur()
      console.log(modaldata)
    });
  
    return await modal.present();
   }

   async openModalNacionalidad() {
    const modal = await this.modalController.create({
    component: ModalnacionalidadesPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.nacionalidadselect = modaldata.text;
      this.nacionalidad.setValue(String(modaldata.text));
      this.onblur()

      console.log(modaldata)
    });
  
    return await modal.present();
   }

   async openModalImage() {
    const modal = await this.modalController.create({
    component: ModalimagePage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      console.log(data)
      this.onimage(data.data);

    });
  
    return await modal.present();
   }

   async Alerta(text: string) {
    const alert = await this.alertController.create({
      message: text,
      buttons: ['OK']
    });

    await alert.present();
  }


  onimage(id:any)
  {
    console.log("id", id)
    if (id == 0)
    {
       this.libro.setpicture(this.camera.PictureSourceType.CAMERA).then(data => {
        this.cropImage(data)
      });
    }
    else{
      if (id == 1)
      {
        this.libro.setpicture(this.camera.PictureSourceType.PHOTOLIBRARY).then(data => {
          this.cropImage(data)
        });
      }
      else{
        if (id == 2){
          this.image = '';
          this.onblur()
        }
      } 
    }
  }



  cropImage(imgPath) {
    this.crop.crop(imgPath, this.cropOptions)
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          console.log(error)
        }
      );
  }

  showCroppedImage(ImagePath) {
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.image = base64;

      this.onblur()
    }, error => {
      console.log(error)
    });
  }
  

}
