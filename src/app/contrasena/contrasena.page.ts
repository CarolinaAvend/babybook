import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-contrasena',
  templateUrl: './contrasena.page.html',
  styleUrls: ['./contrasena.page.scss'],
})
export class ContrasenaPage implements OnInit {
  cambio = false;
  contrasenaform: FormGroup;

  constructor(public formbuilder: FormBuilder,
    private libro: LibroService,) {
    this.contrasenaform = this.formbuilder.group({
      actual: new FormControl('',Validators.required),
      nueva: new FormControl('',Validators.required),
      repetir: new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.cambio = false;
  }

  ionViewDidLeave(){
    this.cambio = false
  }

  oncambio(){
    console.log(this.contrasenaform.value)
    if (this.contrasenaform.valid) {
      this.libro.changepass(this.contrasenaform.value.nueva).subscribe((data:any) => {
        console.log(data);
        this.cambio = true;
      }, error => {
        console.log(error);
      })
    }
    return false;
  }

  get errorControl() {
    return this.contrasenaform.controls;
  }

  

}
