import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalnacionalidadesPageRoutingModule } from './modalnacionalidades-routing.module';

import { ModalnacionalidadesPage } from './modalnacionalidades.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalnacionalidadesPageRoutingModule
  ],
  declarations: [ModalnacionalidadesPage]
})
export class ModalnacionalidadesPageModule {}
