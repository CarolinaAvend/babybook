import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalnacionalidades',
  templateUrl: './modalnacionalidades.page.html',
  styleUrls: ['./modalnacionalidades.page.scss'],
})
export class ModalnacionalidadesPage implements OnInit {
  title = 'Seleccione la nacionalidad';
  opciones = [
    {id:1, texto: 'Argentino'},
    {id:2, texto: 'Beliceño'},
    {id:3, texto: 'Boliviano'},
    {id:4, texto: 'Brasileño'},
    {id:5, texto: 'Canadiense'},
    {id:6, texto: 'Colombiano'},
    {id:7, texto: 'Costarricense'},
    {id:8, texto: 'Cubano'},
    {id:9, texto: 'Chileno'},
    {id:10, texto: 'Dominicano'},
    {id:11, texto: 'Ecuatoriano'},
    {id:12, texto: 'Groenlandés'},
    {id:13, texto: 'Guatemalteco'},
    {id:14, texto: 'Haitiano'},
    {id:15, texto: 'Hondureño'},
    {id:16, texto: 'Jamaicano'},
    {id:17, texto: 'Mexicano'},
    {id:18, texto: 'Nicaragüense'},
    {id:19, texto: 'Panameño'},
    {id:20, texto: 'Paraguayo'},
    {id:21, texto: 'Peruano'},
    {id:22, texto: 'Salvadoreño'},
    {id:23, texto: 'Surinamés'},
    {id:24, texto: 'Estadounidense'},
    {id:25, texto: 'Uruguayo'},
    {id:26, texto: 'Venezolano'},
  ]

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss(opcion: any,texto: any) {
    this.modalCtrl.dismiss({id:opcion,text:texto});

  }

}
