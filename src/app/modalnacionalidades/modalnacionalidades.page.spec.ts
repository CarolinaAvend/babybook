import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalnacionalidadesPage } from './modalnacionalidades.page';

describe('ModalnacionalidadesPage', () => {
  let component: ModalnacionalidadesPage;
  let fixture: ComponentFixture<ModalnacionalidadesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalnacionalidadesPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalnacionalidadesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
