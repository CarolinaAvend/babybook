import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalnacionalidadesPage } from './modalnacionalidades.page';

const routes: Routes = [
  {
    path: '',
    component: ModalnacionalidadesPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalnacionalidadesPageRoutingModule {}
