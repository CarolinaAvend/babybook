import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-privacidad',
  templateUrl: './privacidad.page.html',
  styleUrls: ['./privacidad.page.scss'],
})
export class PrivacidadPage implements OnInit {
  privado = true;

  constructor() { }

  ngOnInit() {
  }

  ToogleChange(valor: boolean) {
    this.privado = !valor;
  }

}
