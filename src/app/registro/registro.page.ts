import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { ModalultimoPage } from '../modalultimo/modalultimo.page';
import { LibroService } from 'src/app/service/libro.service';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { ModalopcionPage } from '../modalopcion/modalopcion.page';


@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {
  registerform: FormGroup;
  generoselect = '';

  constructor(public formbuilder: FormBuilder,
    public modalController: ModalController,
    private libro: LibroService,
    private loadingController: LoadingController,
    public alertController: AlertController,
    private router: Router,
    ) {
    this.registerform = this.formbuilder.group({
      usuarioid: new FormControl('',Validators.compose([Validators.required, Validators.minLength(6)])),
      contrasena: new FormControl('',Validators.compose([Validators.required, Validators.minLength(6)])),
      nombre: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      apellido: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      genero: new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
  }

  get errorControl() {
    return this.registerform.controls;
  }

  async openpoliticas() {
    
    const modal = await this.modalController.create({
    component: ModalultimoPage,
    cssClass: 'my-modal-politica auto-height auto-width',
    showBackdrop: true,
    backdropDismiss: true
    });
    modal.onDidDismiss()
    .then((data) => {
      const terminos = data['data'];
      console.log(terminos)
      console.log("testing")
      if (terminos){
        this.registrar();
      }
      
    });
  
    return await modal.present();
   }   


   registrar(){
    let texto = '';
    console.log(this.registerform.value)
    let genero = Number(this.registerform.value.genero);

    this.libro.registro(this.registerform.value.usuarioid,this.registerform.value.contrasena,this.registerform.value.nombre,this.registerform.value.apellido,genero).subscribe((data: any)=>{
      console.log("Datos enviados")
      console.log(data)
      texto = data.message;
      this.Alerta(texto,true)
    }, error =>{
      console.log(error)
      texto = error.error.message;
      this.Alerta(texto,false)
    } )
    
    
   }


   async Alerta(text: string, valid: boolean) {
    const alert = await this.alertController.create({
      message: text,
      cssClass: 'alert-css',
      buttons: [
          {
            text: 'OK',
            handler: () => {
              if(valid){
                this.Loading();
              }
            }
          }, 
      ]
    });

    await alert.present();
  }

  async Loading() {
    const loading = await this.loadingController.create({
      duration: 2000,
      message: 'Cargando...',
      cssClass: 'custom-class custom-loading',
    });
    await loading.present();

    const { data } = await loading.onDidDismiss();
    this.router.navigate(['login'])
  }


  async openModalOpcion() {
    const modal = await this.modalController.create({
    component: ModalopcionPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.generoselect = modaldata.text;
      this.registerform.controls['genero'].setValue(String(modaldata.id));

      console.log(modaldata)
      console.log(this.registerform.value)
    });
  
    return await modal.present();
   }

   onKeyUp(event: any, tipo: number) {

    let newValue = event.target.value;
  
    let regExp = new RegExp('^[A-Za-z? ]+$');
  
    if (! regExp.test(newValue)) {
      event.target.value = newValue.slice(0, -1);
  
      if (tipo == 0){
        this.registerform.controls['nombre'].setValue(newValue.slice(0, -1))
      }
  
      if (tipo == 1){
        this.registerform.controls['apellido'].setValue(newValue.slice(0, -1))
      }
    }
  }
}


