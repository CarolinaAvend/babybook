import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { ModalultimoPageModule } from '../modalultimo/modalultimo.module';
import { ModalopcionPageModule } from '../modalopcion/modalopcion.module';

import { IonicModule } from '@ionic/angular';

import { RegistroPageRoutingModule } from './registro-routing.module';

import { RegistroPage } from './registro.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistroPageRoutingModule,
    ReactiveFormsModule,
    ModalultimoPageModule,
    ModalopcionPageModule
  ],
  declarations: [RegistroPage]
})
export class RegistroPageModule {}
