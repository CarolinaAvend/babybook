import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import * as jimp from 'jimp';

@Component({
  selector: 'app-preview',
  templateUrl: './preview.page.html',
  styleUrls: ['./preview.page.scss'],
})
export class PreviewPage implements OnInit {
  hijos: any;
  nombres = '';
  countname = 0;
  isKeyboardHide=true;
  fotos = []

  constructor(private route: ActivatedRoute, 
    private router: Router) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.hijos = this.router.getCurrentNavigation().extras.state.hijos;
          this.fotos = this.router.getCurrentNavigation().extras.state.foto;
        }
      });
    }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.nombres = ''
    this.countname = 0
    this.setnombres();
  }

  setnombres(){
    this.hijos.forEach(element => {
      if (element.seleccionado)
      {
        if (this.countname > 0){
          this.nombres = this.nombres + ', ' + element.nombre
        }else{
          this.nombres =  element.nombre
        }
        this.countname = this.countname + 1
      }
    });
  }

}
