import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ModalopcionPage } from './modalopcion.page';

describe('ModalopcionPage', () => {
  let component: ModalopcionPage;
  let fixture: ComponentFixture<ModalopcionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalopcionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ModalopcionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
