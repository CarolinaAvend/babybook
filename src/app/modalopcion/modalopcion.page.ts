import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modalopcion',
  templateUrl: './modalopcion.page.html',
  styleUrls: ['./modalopcion.page.scss'],
})
export class ModalopcionPage implements OnInit {
  title = 'Seleccione su genero';
  opciones = [
    {id:1, texto: 'Hombre'},
    {id:2, texto: 'Mujer'},
    {id:3, texto: 'Otro'},
  ]


  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  dismiss(opcion: any,texto: any) {
    this.modalCtrl.dismiss({id:opcion,text:texto});

  }

}
