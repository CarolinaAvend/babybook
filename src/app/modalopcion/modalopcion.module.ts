import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ModalopcionPageRoutingModule } from './modalopcion-routing.module';

import { ModalopcionPage } from './modalopcion.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ModalopcionPageRoutingModule
  ],
  declarations: [ModalopcionPage]
})
export class ModalopcionPageModule {}
