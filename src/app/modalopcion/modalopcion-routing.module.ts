import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ModalopcionPage } from './modalopcion.page';

const routes: Routes = [
  {
    path: '',
    component: ModalopcionPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ModalopcionPageRoutingModule {}
