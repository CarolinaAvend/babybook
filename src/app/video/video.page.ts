import { Component, OnInit } from '@angular/core';
import { CameraPreview, CameraPreviewOptions } from '@ionic-native/camera-preview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { LibroService } from 'src/app/service/libro.service';
import { AlertController } from '@ionic/angular';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-video',
  templateUrl: './video.page.html',
  styleUrls: ['./video.page.scss'],
})
export class VideoPage implements OnInit {
  hijos: any;
  nombres = '';
  countname = 0;
  isKeyboardHide=true;
  grabando = false;

  cameraPreviewOpts: CameraPreviewOptions = {
    x: 0, 
    y: 0, 
    width: window.screen.width, 
    height: window.screen.height - 170,
    camera: 'rear',
    tapPhoto: false,
    previewDrag: false,
    toBack: true,
    alpha: 1
  }
  alternar = false;
  flash = false;
  IMAGE_PATH: any;

  optionsvideo = {
    width: (window.screen.width / 2),
    height: (window.screen.height / 2),
    quality: 60,
    withFlash: false
  }

  constructor(private cameraPreview: CameraPreview,
    private camera: Camera,
    private libro: LibroService,
    private route: ActivatedRoute, 
    private router: Router,
    public alertController: AlertController,
    public keyboard:Keyboard) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.hijos = this.router.getCurrentNavigation().extras.state.hijos;
        }
      });
    }

    

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.nombres = ''
    this.countname = 0
    this.setnombres();
    this.cameraPreview.startCamera(this.cameraPreviewOpts).then(
      (res) => {
        console.log(res)
      },
      (err) => {
        console.log(err)
      });
      this.keyboard.onKeyboardWillShow().subscribe(()=>{
        this.isKeyboardHide=false;
         console.log('SHOWK');
      });
  
      this.keyboard.onKeyboardWillHide().subscribe(()=>{
        this.isKeyboardHide=true;
        console.log('HIDEK');
      });
  }

  setnombres(){
    this.hijos.forEach(element => {
      if (element.seleccionado)
      {
        if (this.countname > 0){
          this.nombres = this.nombres + ', ' + element.nombre
        }else{
          this.nombres =  element.nombre
        }
        this.countname = this.countname + 1
      }
    });
  }

  changecamera(){
    this.cameraPreview.switchCamera();
    if(this.alternar){
      this.alternar = false;
    }else{
      this.alternar = true;
    }
  }

  grabar(){
    //solo android
    if (this.grabando)
    {
      this.cameraPreview.stopRecordVideo().then((filePath) => {
        console.log(filePath)    
      });
      this.grabando = false
    }else{
      this.grabando = true
      this.cameraPreview.startRecordVideo(this.optionsvideo).then((filePath) => {
        console.log(filePath)    
      });
    }
  }


  gofoto(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/foto'], navigationExtras)
  }

  gotexto(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/texto'], navigationExtras)
  }

  gopublicar(){
    //let navigationExtras: NavigationExtras = { state: { hijos: this.hijos, video: 'test' } };
    //this.router.navigate(['/publicar/video'], navigationExtras)
    this.Alerta('No disponible')
  }

  async Alerta(text: string) {
    const alert = await this.alertController.create({
      message: text,
      buttons: ['OK']
    });

    await alert.present();
  }

}
