import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CameraPreview } from '@ionic-native/camera-preview/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

import { IonicModule } from '@ionic/angular';

import { FotoPageRoutingModule } from './foto-routing.module';

import { FotoPage } from './foto.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FotoPageRoutingModule,
    FontAwesomeModule
  ],
  declarations: [FotoPage],
  providers: [
    CameraPreview,
    ImagePicker
  ],
})
export class FotoPageModule {}
