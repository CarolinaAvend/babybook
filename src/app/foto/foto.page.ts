import { Component, OnInit } from '@angular/core';
import { CameraPreview, CameraPreviewOptions } from '@ionic-native/camera-preview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { LibroService } from 'src/app/service/libro.service';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { ImagePicker } from '@ionic-native/image-picker/ngx';

@Component({
  selector: 'app-foto',
  templateUrl: './foto.page.html',
  styleUrls: ['./foto.page.scss'],
})
export class FotoPage implements OnInit {
  hijos: any;
  nombres = '';
  countname = 0;
  isKeyboardHide=true;
  imagenes = [
    "http://localhost:3000/uploads/20190921_185318.jpg",
    "http://localhost:3000/uploads/20190928_165840.jpg",
    "http://localhost:3000/uploads/20190928_165235.jpg",
    "http://localhost:3000/uploads/20200916_190007.jpg",
  ]

  optionsgallery = {
    maximumImagesCount: 8,
    targetWidth: 1080,
    targetHeight: 1080,
    quality: 85,
    outputType: 1
  };

  cameraPreviewOpts: CameraPreviewOptions = {
    x: 0, 
    y: 0, 
    width: window.screen.width, 
    height: window.screen.height - 170,
    camera: 'rear',
    tapPhoto: false,
    previewDrag: false,
    toBack: true,
    alpha: 1,
  }
  alternar = false;
  flash = false;
  IMAGE_PATH: any;
  heightaux;

  options: CameraOptions = {
    quality: 85,
    targetWidth: 1080,
    targetHeight: 1080,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    correctOrientation: true,
    saveToPhotoAlbum: true,
  }

  constructor(private cameraPreview: CameraPreview,
    private camera: Camera,
    private libro: LibroService,
    private route: ActivatedRoute, 
    private router: Router,
    public keyboard:Keyboard,
    private imagePicker: ImagePicker) {
      this.route.queryParams.subscribe(params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.hijos = this.router.getCurrentNavigation().extras.state.hijos;
        }
      });
    }

  ngOnInit() {

  }

  ionViewWillEnter(){
    this.IMAGE_PATH = ''
    this.nombres = ''
    this.countname = 0
    this.setnombres();
    this.cameraPreview.startCamera(this.cameraPreviewOpts).then(
      (res) => {
        this.cameraPreview.setFlashMode('off')
        console.log(res)
      },
      (err) => {
        console.log(err)
      });
      this.keyboard.onKeyboardWillShow().subscribe(()=>{
        this.isKeyboardHide=false;
         console.log('SHOWK');
      });
  
      this.keyboard.onKeyboardWillHide().subscribe(()=>{
        this.isKeyboardHide=true;
        console.log('HIDEK');
      });
  }

  setnombres(){
    this.hijos.forEach(element => {
      if (element.seleccionado)
      {
        if (this.countname > 0){
          this.nombres = this.nombres + ', ' + element.nombre
        }else{
          this.nombres =  element.nombre
        }
        this.countname = this.countname + 1
      }
    });
  }

  changecamera(){
    this.cameraPreview.switchCamera();
    if(this.alternar){
      this.alternar = false;
    }else{
      this.alternar = true;
    }
  }

  onflash(){
    if(this.flash){
      this.flash = false;
      this.cameraPreview.setFlashMode('off')
    }else{
      this.flash = true;
      this.cameraPreview.setFlashMode('on')
    }
  }

  gallery(){
    console.log("getimage")
    this.imagePicker.getPictures(this.optionsgallery).then((results) => {
      for (var i = 0; i < results.length; i++) {
        console.log('Image URI: ' + results[i]);
        this.imagenes.push('data:image/jpeg;base64,' + results[i])
      }
      console.log(this.imagenes)
      if (this.imagenes.length > 0){
        this.gopublicar()
        //this.gopreview()
      }
    }, (err) => { });

  }

  tomarfoto(){
    this.cameraPreview.takePicture({
      width: 1080,
      height: 1080,
      quality: 85,
    }).then((imageData) => {
      this.cameraPreview.stopCamera();
      this.IMAGE_PATH = 'data:image/jpeg;base64,' + imageData;
      this.imagenes.push(this.IMAGE_PATH)
      this.gopublicar();
      //this.gopreview();
    }, (err) => {
      console.log(err);
    });
  }

  gotexto(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/texto'], navigationExtras)
  }

  govideo(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos } };
    this.router.navigate(['/video'], navigationExtras)
  }

  gopublicar(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos, foto: this.imagenes } };
    this.router.navigate(['/publicar/foto'], navigationExtras)
  }


  gopreview(){
    let navigationExtras: NavigationExtras = { state: { hijos: this.hijos, foto: this.imagenes } };
    this.router.navigate(['/preview'], navigationExtras)
  }


}
