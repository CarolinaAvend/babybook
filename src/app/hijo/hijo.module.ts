import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { Ionic4DatepickerModule } from '@logisticinfotech/ionic4-datepicker';
import { ModalopcionPageModule } from '../modalopcion/modalopcion.module';
import { ModalnacionalidadesPageModule } from '../modalnacionalidades/modalnacionalidades.module';
import { ModalimagePageModule } from '../modalimage/modalimage.module';

import { IonicModule } from '@ionic/angular';

import { HijoPageRoutingModule } from './hijo-routing.module';

import { HijoPage } from './hijo.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HijoPageRoutingModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    Ionic4DatepickerModule,
    ModalopcionPageModule,
    ModalnacionalidadesPageModule,
    ModalimagePageModule
  ],
  declarations: [HijoPage]
})
export class HijoPageModule {}
