import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LibroService } from 'src/app/service/libro.service';
import { AlertController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { ModalopcionPage } from '../modalopcion/modalopcion.page';
import { ModalnacionalidadesPage } from '../modalnacionalidades/modalnacionalidades.page';
import { ModalController } from '@ionic/angular';
import { ModalimagePage } from '../modalimage/modalimage.page';
import { Camera } from '@ionic-native/camera/ngx';
import { Crop,CropOptions } from '@ionic-native/crop/ngx';
import { File } from '@ionic-native/file/ngx';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.page.html',
  styleUrls: ['./hijo.page.scss'],
})
export class HijoPage implements OnInit {
  hijoform: FormGroup;
  generoselect = '';
  nacionalidadselect = '';
  masdatos = false;
  image = ''
  selectedDate;
  disabledDates: Date[] = [];
  datePickerObj: any = {
    inputDate: new Date(), 
    fromDate: new Date('1950-01-01'),
    toDate: new Date('2100-01-01'), 
    closeOnSelect: true,
    mondayFirst: true, 
    todayLabel: 'Hoy', 
    closeLabel: 'Cerrar', 
    titleLabel: 'Seleccionar Fecha',
    monthsList: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
    weeksList: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
    dateFormat: 'YYYY-MM-DD', 
    clearButton : false , 
    momentLocale: 'es-CL', 
    yearInAscending: true, 
    btnCloseSetInReverse: true, 
    btnProperties: {
      expand: 'block', 
      fill: '', 
      size: '', 
      disabled: '', 
      strong: '', 
      color: ''
    },
    arrowNextPrev: {}, 
    highlightedDates: [],
    isSundayHighlighted : {
     fontColor: '#ee88bf' } 
  };

  id;
  apiurl;
  hijo = {
    nombre: '',
    apellido: '',
    materno: '',
    sexo: 0,
    fecha: new Date(),
    peso: '',
    talla: '',
    apgar: '',
    lugar: '',
    nacionalidad: '',
    pelo: '',
    ojos: '',
    parecido: '',
    direccion: 'this.hijoform.value.direccion',
    foto:''
  };
  hijoaux = {
    nombre: '',
    apellido: '',
    materno: '',
    sexo: 0,
    fecha: new Date(),
    peso: '',
    talla: '',
    apgar: '',
    lugar: '',
    nacionalidad: '',
    pelo: '',
    ojos: '',
    parecido: '',
    direccion: 'this.hijoform.value.direccion',
    foto:''
  }

  cropOptions: CropOptions = {
    quality: 50
  }

  constructor(
    public formbuilder: FormBuilder,
    public alertController: AlertController,
    private libro: LibroService,
    private router: Router,
    public modalController: ModalController,
    private loadingController: LoadingController,
    private camera: Camera,
    private crop: Crop,
    private file: File,
    private activatedRoute: ActivatedRoute,
  ) {
    this.hijoform = this.formbuilder.group({
      nombre: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      apellido: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      materno: new FormControl('',Validators.compose([Validators.required, Validators.minLength(3)])),
      sexo: new FormControl('',Validators.required),
      fecha: new FormControl('',Validators.required),
      peso: new FormControl(''),
      talla: new FormControl(''),
      apgar: new FormControl(''),
      lugar: new FormControl(''),
      nacionalidad: new FormControl(''),
      pelo: new FormControl(''),
      ojos: new FormControl(''),
      parecido: new FormControl(''),
      direccion: new FormControl(''),
      foto: new FormControl(''),
    })

    this.id = this.activatedRoute.snapshot.paramMap.get('id')
  }

  ngOnInit() {
    console.log('pagina de hijo')
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.gethijo();
  }

  ionViewWillLeave(){
    this.masdatos = false;    
  }

  mostrarmasdatos(){
    this.masdatos = true;
  }

  async Alerta(text: string) {
    const alert = await this.alertController.create({
      message: text,
      buttons: ['OK']
    });

    await alert.present();
  }

  async Loading() {
    const loading = await this.loadingController.create({
      duration: 2000,
      message: 'Cargando...',
      cssClass: 'custom-class custom-loading',
    });
    await loading.present();

    const { data } = await loading.onDidDismiss();
    this.router.navigate(['tabs/tab5'])
  }
  
  async openModalOpcion() {
    const modal = await this.modalController.create({
    component: ModalopcionPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.generoselect = modaldata.text;
      this.hijoform.controls['sexo'].setValue(String(modaldata.id));

      console.log(modaldata)
      console.log(this.hijoform.value)
    });
  
    return await modal.present();
   }

   async openModalNacionalidad() {
    const modal = await this.modalController.create({
    component: ModalnacionalidadesPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      const modaldata = data['data'];
      this.nacionalidadselect = modaldata.text;
      this.hijoform.controls['nacionalidad'].setValue(String(modaldata.text));

      console.log(modaldata)
      console.log(this.hijoform.value)
    });
  
    return await modal.present();
   }

   async openModalImage() {
    const modal = await this.modalController.create({
    component: ModalimagePage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
      this.onimage(data.data);
    });
  
    return await modal.present();
   }

   onimage(id:any)
   {
     console.log("id", id)
     if (id == 0)
     {
        this.libro.setpicture(this.camera.PictureSourceType.CAMERA).then(data => {
         this.cropImage(data)
       });
     }
     else{
       if (id == 1)
       {
         this.libro.setpicture(this.camera.PictureSourceType.PHOTOLIBRARY).then(data => {
           this.cropImage(data)
         });
       }
       else{
         if (id == 2){
           this.image = '';
         }
       } 
     }
   }

   cropImage(imgPath) {
    this.crop.crop(imgPath, this.cropOptions)
      .then(
        newPath => {
          this.showCroppedImage(newPath.split('?')[0])
        },
        error => {
          console.log(error)
        }
      );
  }

  showCroppedImage(ImagePath) {
    var copyPath = ImagePath;
    var splitPath = copyPath.split('/');
    var imageName = splitPath[splitPath.length - 1];
    var filePath = ImagePath.split(imageName)[0];

    this.file.readAsDataURL(filePath, imageName).then(base64 => {
      this.image = base64;
    }, error => {
      console.log(error)
    });
  }


  gethijo(){
    console.log('getson')
    let aux = Number(this.id)
    this.libro.getson(aux).subscribe((result:any) => {
      console.log(result)
      if (result.data[0].foto != ''){
        this.hijoform.controls['foto'].setValue(this.apiurl + result.data[0].foto);
        this.image = this.apiurl + result.data[0].foto;
      }else{
        this.hijoform.controls['foto'].setValue('');
        this.image = '';
      }
      this.hijoform.controls['nombre'].setValue(result.data[0].nombre);
      this.hijoform.controls['apellido'].setValue(result.data[0].apellido_paterno);
      this.hijoform.controls['materno'].setValue(result.data[0].apellido_materno);
      this.hijoform.controls['sexo'].setValue(result.data[0].genero);
      this.setgenero();
      this.hijoform.controls['fecha'].setValue(formatDate(new Date(result.data[0].fecha_nacimiento), 'yyyy-MM-dd','es-CL'));
      this.hijoform.controls['peso'].setValue(result.data[0].peso);
      this.hijoform.controls['talla'].setValue(result.data[0].talla);
      this.hijoform.controls['apgar'].setValue(result.data[0].apgar);
      this.hijoform.controls['lugar'].setValue(result.data[0].lugar_nacimiento);
      this.hijoform.controls['nacionalidad'].setValue(result.data[0].nacionalidad);
      this.nacionalidadselect = result.data[0].nacionalidad;
      this.hijoform.controls['pelo'].setValue(result.data[0].color_pelo);
      this.hijoform.controls['ojos'].setValue(result.data[0].color_ojos);
      this.hijoform.controls['parecido'].setValue(result.data[0].parecido);
      this.hijoform.controls['direccion'].setValue(result.data[0].direccion);
    }, error =>{
      console.log(error)
    })
  }

  setgenero(){
    if (this.hijoform.value.sexo == 1)
    {
      this.generoselect = 'Hombre';
    }
    else {
      if (this.hijoform.value.sexo == 2)
      {
        this.generoselect = 'Mujer';
      }
      else{
        this.generoselect = 'Otro';
      }
    }
  }

  SetHijo(){
    this.hijo = {
      nombre: this.hijoform.value.nombre,
      apellido: this.hijoform.value.apellido_paterno,
      materno: this.hijoform.value.apellido_materno,
      sexo: this.hijoform.value.genero,
      fecha: this.hijoform.value.fecha_nacimiento,
      peso: this.hijoform.value.peso,
      talla: this.hijoform.value.talla,
      apgar: this.hijoform.value.apgar,
      lugar: this.hijoform.value.lugar_nacimiento,
      nacionalidad: this.hijoform.value.nacionalidad,
      pelo: this.hijoform.value.color_pelo,
      ojos: this.hijoform.value.color_ojos,
      parecido: this.hijoform.value.parecido,
      direccion: this.hijoform.value.direccion,
      foto: this.image
    }
  }

  SetHijoaux(){
    this.hijoaux = {
      nombre: this.hijoform.value.nombre,
      apellido: this.hijoform.value.apellido_paterno,
      materno: this.hijoform.value.apellido_materno,
      sexo: this.hijoform.value.genero,
      fecha: this.hijoform.value.fecha_nacimiento,
      peso: this.hijoform.value.peso,
      talla: this.hijoform.value.talla,
      apgar: this.hijoform.value.apgar,
      lugar: this.hijoform.value.lugar_nacimiento,
      nacionalidad: this.hijoform.value.nacionalidad,
      pelo: this.hijoform.value.color_pelo,
      ojos: this.hijoform.value.color_ojos,
      parecido: this.hijoform.value.parecido,
      direccion: this.hijoform.value.direccion,
      foto:this.image
    }
  }

  edithijo(){
    let texto = ''
    console.log("edituser")
    this.libro.editson(this.hijo).subscribe((data:any) => {
      console.log(data)
      texto = data.message;
      this.Alerta(texto)
      this.SetHijo()
      this.SetHijoaux()
    }, error =>{
      console.log(error)
      texto = error.error.message;
      this.Alerta(texto)
    })
  }

  haycambios(){
    let cambios = false
    if (this.hijoaux.foto != this.image && this.hijo.foto != null && this.image != null){cambios = true}
    if (this.hijoaux.nombre != this.hijo.nombre){cambios = true}
    if (this.hijoaux.apellido != this.hijo.apellido){cambios = true}
    if (this.hijoaux.materno != this.hijo.materno){cambios = true}
    if (this.hijoaux.sexo != Number(this.hijo.sexo)){cambios = true}
    if (this.hijoaux.fecha != this.hijo.fecha && this.hijo.fecha != null){cambios = true}
    if (this.hijoaux.nacionalidad != this.hijo.nacionalidad){cambios = true}
    if (this.hijoaux.peso != this.hijo.peso){cambios = true}
    if (this.hijoaux.talla != this.hijo.talla){cambios = true}
    if (this.hijoaux.apgar != this.hijo.apgar){cambios = true}
    if (this.hijoaux.lugar != this.hijo.lugar){cambios = true}
    if (this.hijoaux.pelo != this.hijo.pelo){cambios = true}
    if (this.hijoaux.ojos != this.hijo.ojos){cambios = true}
    if (this.hijoaux.parecido != this.hijo.parecido){cambios = true}
    if (this.hijoaux.direccion != this.hijo.direccion){cambios = true}
    if(cambios){
      this.edithijo()
    }
  }

  onblur(){
    console.log("ionblur")
    this.SetHijo();
    this.haycambios();
  }

}
