import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HijoPage } from './hijo.page';

describe('HijoPage', () => {
  let component: HijoPage;
  let fixture: ComponentFixture<HijoPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HijoPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HijoPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
