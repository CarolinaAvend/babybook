import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.page.html',
  styleUrls: ['./editar.page.scss'],
})
export class EditarPage implements OnInit {
  id = '';

  constructor(private router: Router,
    private libro: LibroService,
    private activatedRoute: ActivatedRoute,) {
      this.id = this.activatedRoute.snapshot.paramMap.get('id')
    }

  ngOnInit() {
  }

}
