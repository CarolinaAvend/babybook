import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';
import { FormControl, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-comentarios',
  templateUrl: './comentarios.page.html',
  styleUrls: ['./comentarios.page.scss'],
})
export class ComentariosPage implements OnInit {
  comentarios;
  usuario;
  id;
  descripcion;
  imagen;
  postfecha;
  totalcomentario;
  apiurl;
  heart = false;

  comentario = new FormControl('',Validators.required);

  constructor(private libro: LibroService,
    public modalCtrl: ModalController) { }

  ngOnInit() {
  }

  ionViewWillEnter(){

  }

  comentar(){
    console.log(this.comentario)
    let texto = this.comentario.value
    console.log(texto)
    this.libro.addcomment(texto,this.id).subscribe((data:any) => {
      console.log(data)
      this.comentarios.push({
          author_id: this.id,
          author_name: this.usuario,
          author_img: this.imagen,
          create_at: new Date(),
          id: 0,
          text: texto})
      this.comentario.setValue('')
      this.totalcomentario = this.totalcomentario + 1
    },error =>{
      console.log(error)
    })
  }



}

