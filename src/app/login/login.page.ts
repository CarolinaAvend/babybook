import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { LibroService } from 'src/app/service/libro.service';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Keyboard } from '@ionic-native/keyboard/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  isSubmitted = false;
  login: FormGroup;
  isKeyboardHide=true;

  constructor(
    public formbuilder: FormBuilder,
    public alertController: AlertController,
    private libro: LibroService,
    private router: Router,
    public keyboard:Keyboard
  ) { 
    this.login = this.formbuilder.group({
      usuario: new FormControl('', Validators.required),
      contrasena: new FormControl('',Validators.required)
    })
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.keyboard.onKeyboardWillShow().subscribe(()=>{
      this.isKeyboardHide=false;
       console.log('SHOWK');
    });

    this.keyboard.onKeyboardWillHide().subscribe(()=>{
      this.isKeyboardHide=true;
      console.log('HIDEK');
    });
  }


  onlogin(){
    let texto = '';
    this.isSubmitted = true;
    console.log(this.login.value)
    if (!this.login.valid){
      return false;
    }else{
      this.libro.login(this.login.value.usuario,this.login.value.contrasena).subscribe((data: any)=>{
        console.log("Datos enviados")
        console.log(data)
        localStorage.setItem('token',data.token);
        this.login.controls['contrasena'].setValue('');
        this.router.navigate(['/tabs/tab1/all'])
      }, error =>{
        console.log(error)
        texto = error.error.message;
        this.Alerta(texto)
      } )      
    }
  }


  async Alerta(text: string) {
    const alert = await this.alertController.create({
      message: text,
      buttons: ['OK']
    });

    await alert.present();
  }

}
