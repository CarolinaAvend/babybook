import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.page.html',
  styleUrls: ['./feed.page.scss'],
})
export class FeedPage implements OnInit {
  hijos = []
  apiurl = '';

  constructor(public modalCtrl: ModalController,
    private router: Router,
    private libro: LibroService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.hijos = [];
    this.gethijos();
  }

  dismiss(tipo: string) {
    this.modalCtrl.dismiss();
    this.router.navigate(['/tabs/tab1/', tipo])
  }
  
  dismissagregar() {
    this.modalCtrl.dismiss();
    this.router.navigate(['/tabs/agregar'])
  }

  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        this.hijos.push({id: element.id, nombre:element.nombre, foto: element.foto})
      });
    }, error =>{
      console.log(error)
    } )      
  }

}
