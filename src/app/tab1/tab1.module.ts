import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Tab1Page } from './tab1.page';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { OpcionesPageModule } from '../opciones/opciones.module';
import { ComentariosPageModule } from '../comentarios/comentarios.module';

import { Tab1PageRoutingModule } from './tab1-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    Tab1PageRoutingModule,
    OpcionesPageModule,
    ComentariosPageModule
  ],
  declarations: [Tab1Page]
})
export class Tab1PageModule {}
