import { Component } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { OpcionesPage } from '../opciones/opciones.page';
import { ComentariosPage } from '../comentarios/comentarios.page';
import { LibroService } from 'src/app/service/libro.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
    id = '';
    posts = []
    timetemp = 'Hace 2 días';
    apiurl = '';
    user = '';
    img = '';
    aux = [];
    userid = 0;
    usuario = {
      id:0,
      username:' ', 
      img: '',
    }
    likecontrol = []
    all_post = []
    page_number = 0;
    page_limit = 5;
    totaldata = false;

slideOpts = {
    initialSlide: 0,
    speed: 400
};  

  constructor(public modalController: ModalController,
    private libro: LibroService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public toastController: ToastController) {
        this.id = this.activatedRoute.snapshot.paramMap.get('id')
    }

    ionViewWillEnter(){
      this.id = this.activatedRoute.snapshot.paramMap.get('id')
      this.apiurl = this.libro.geturl()
      this.totaldata = false
      console.log(this.totaldata)
      this.all_post = []
      this.likecontrol = [];
      this.page_number = 0;
      this.posts = []
      this.getuser();
    if(this.id != 'all')
        {
          this.getsonpost(this.id)
        }else{
          this.getallpost()
        } 
    }

    ionViewWillLeave(){
      this.totaldata = false
    }

    getuser(){
        this.libro.getme().subscribe((result:any) => {
          console.log(result)
          this.usuario = {id:result.data[0].id, username:result.data[0].name + ' ' + result.data[0].lastname, img:result.data[0].picture}
          this.userid = result.data[0].id
          this.user = result.data[0].name + ' ' + result.data[0].lastname
          this.img = result.data[0].picture
          console.log(this.usuario)
        }, error =>{
          console.log(error)
        })
        console.log(this.usuario)
      }


  async openopcion() {
    const modal = await this.modalController.create({
    component: OpcionesPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
    });
  
    return await modal.present();
   }

   async opencomentarios(index: any) {
    let text
    if (this.posts[index].pictures && this.posts[index].pictures.length > 0){
      text = this.posts[index].description
    }else{
      text = this.posts[index].title
    }
    const modal = await this.modalController.create({
    component: ComentariosPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true,
    componentProps: { 
        comentarios: this.posts[index].comments,
        usuario: this.user,
        id: this.posts[index].id,
        descripcion: text,
        imagen: this.img,
        postfecha: this.posts[index].fecha,
        totalcomentario:this.posts[index].comments_count,
        apiurl: this.apiurl
      }
    });
  
    modal.onDidDismiss()
    .then((data) => {
    });
  
    return await modal.present();
   } 


   like(id: any, index:any){
    if (!this.likecontrol[index]){
      this.libro.addlike(id).subscribe((data: any) => {
        console.log(data)
        this.posts[index].likes_count = this.posts[index].likes_count + 1
        this.likecontrol[index] = true
      }, error => {
        console.log(error)
      })
    }
  }

   getallpost(){
    this.libro.getpost().subscribe((result:any) => {
      console.log(result);
      this.all_post = result[0].data
      this.posts = this.all_post.slice(0, 5);
      console.log('post',this.posts)
      this.controlike()
    }, error => {
      console.log(error);
    })
  }

  getsonpost(id: string){
    this.libro.getpostson(Number(id)).subscribe((result:any) => {
      console.log(result);
      this.all_post = result[0].data
      this.posts = this.all_post.slice(0, 5);
      console.log('post',this.posts)
      this.controlike()
    },error => {
      console.log(error)
    })
  }

  controlike(){
    let i = 0;
    let temp;
    for (i = 0;i < this.posts.length; i++)
    {
        temp = this.posts[i].likes
        this.likecontrol[i] = false
        for(let j=0;j<temp.length;j++)
        {
            if(temp[j].author_id == this.userid){
                this.likecontrol[i] = true
            }
        }
    }
  }

  doInfinite(event) {
    this.getinfinitopost(true,event)
  }

  getinfinitopost(isFirstLoad, event) {
    let inicio = this.posts.length
    let final = this.posts.length + 5
    if (final > this.all_post.length){
      final = this.all_post.length
    }
    if (this.page_number <= this.page_limit){
        for (let i = inicio; i < final; i++) {
          this.posts.push(this.all_post[i]);
        }
        if (isFirstLoad)
          event.target.complete();

        this.page_number++;
    }else{
      event.target.complete();
      this.totaldata = true
    }
    if (inicio == final)
    {
      event.target.complete();
      this.totaldata = true
    }
    console.log(this.posts)
  }

  doRefresh(event) {
    let newpost = []
    if(this.id != 'all')
    {
      this.libro.getpostson(this.id).subscribe((result:any) => {
        newpost = result[0].data
        console.log(result)
      },error =>{
        console.log(error)
      })
    }else{
      this.libro.getpost().subscribe((result:any) => {
        newpost = result[0].data
        console.log(result)
      },error =>{
        console.log(error)
      })
    }
    console.log("temp:", newpost)
    if (this.all_post.length == newpost.length) {
      this.presentToast()
      event.target.complete();

    } else {
      event.target.complete();
      this.likecontrol = [];
      this.all_post = newpost
      this.posts = this.all_post.slice(0, 5);
      this.page_number = 0;
      this.totaldata = false;
      this.controlike()
    }
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'No hay nuevos post',
      position: 'top',
      duration: 1500
    });
    toast.present();
  }

}