import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ImagenopPage } from './imagenop.page';

describe('ImagenopPage', () => {
  let component: ImagenopPage;
  let fixture: ComponentFixture<ImagenopPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImagenopPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ImagenopPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
