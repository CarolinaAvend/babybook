import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ImagenopPage } from './imagenop.page';

const routes: Routes = [
  {
    path: '',
    component: ImagenopPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImagenopPageRoutingModule {}
