import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ImagenopPageRoutingModule } from './imagenop-routing.module';

import { ImagenopPage } from './imagenop.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ImagenopPageRoutingModule
  ],
  declarations: [ImagenopPage]
})
export class ImagenopPageModule {}
