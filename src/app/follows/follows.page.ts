import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-follows',
  templateUrl: './follows.page.html',
  styleUrls: ['./follows.page.scss'],
})
export class FollowsPage implements OnInit {
  seguidores = []
  total = 0;
  usuario = {
    id:0,
    nombre:'',
    apellido: '',
    img: '',
  }
  hijos = []
  apiurl = ''

  constructor(private libro: LibroService) { }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.seguidores = [];
    this.total = 0;
    this.hijos = []
    this.setseguidos()
    this.getuser()
    this.gethijos()
  }

  setseguidos(){
    this.libro.follows().subscribe((result: any)=>{
      console.log(result)
      if (result.data.length > 0){
        result.data.forEach(element => {
          this.seguidores.push({id:element.id, nombre:element.name, apellido:element.lastname, img: element.picture})
          this.total = this.seguidores.length
        });
      }
    }, error =>{
      console.log(error)
    } )
  }

  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        if (element.foto != ''){
          this.hijos.push({id: element.id, nombre:element.nombre, foto: this.apiurl + element.foto})
        }else{
          this.hijos.push({id: element.id, nombre:element.nombre, foto: '/assets/img/ico-user-line-36x36.svg'})
        }
      });
    }, error =>{
      console.log(error)
    } )      
  }

  getuser(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      this.usuario = {id:result.data[0].id, nombre:result.data[0].name, apellido: result.data[0].lastname, img: result.data[0].picture}
    }, error =>{
      console.log(error)
    })
  }
  
}
