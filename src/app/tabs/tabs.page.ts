import { Component } from '@angular/core';

import { SelectorPage } from '../selector/selector.page'
import { FeedPage } from '../feed/feed.page'
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  select = false;
  notify_number = 1;

  constructor(
    public modalController: ModalController,
    private router: Router,) {}




  async openModal() {
    const modal = await this.modalController.create({
    component: FeedPage,
    cssClass: 'my-modal auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
    });
  
    return await modal.present();
   }

  async openModalmultiple() {
    const modal = await this.modalController.create({
    component: SelectorPage,
    cssClass: 'my-modal-multi auto-height bottom',
    showBackdrop: true,
    backdropDismiss: true
    });
  
    modal.onDidDismiss()
    .then((data) => {
    });
  
    return await modal.present();
   }

  goperfil(){
    this.router.navigate(['/tabs/tab5'])
  }



}


