import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { SelectorPageModule } from '../selector/selector.module'
import { FeedPageModule } from '../feed/feed.module'

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
    FontAwesomeModule,
    SelectorPageModule,
    FeedPageModule
  ],
  declarations: [TabsPage]
})
export class TabsPageModule {}
