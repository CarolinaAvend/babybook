import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'tab1/:id',
        loadChildren: () => import('../tab1/tab1.module').then(m => m.Tab1PageModule)
      },
      {
        path: 'tab2',
        loadChildren: () => import('../tab2/tab2.module').then(m => m.Tab2PageModule)
      },
      {
        path: 'tab3',
        loadChildren: () => import('../tab3/tab3.module').then(m => m.Tab3PageModule)
      },
      {
        path: 'tab4',
        loadChildren: () => import('../tab4/tab4.module').then( m => m.Tab4PageModule)
      },
      {
        path: 'tab5',
        loadChildren: () => import('../tab5/tab5.module').then( m => m.Tab5PageModule)
      },
      { 
        path: 'muro/:tipo/:id',
        loadChildren: () => import('../muro/muro.module').then(m => m.MuroPageModule)
      },
      {
        path: 'seguidores',
        loadChildren: () => import('../seguidores/seguidores.module').then( m => m.SeguidoresPageModule)
      },
      {
        path: 'libro',
        loadChildren: () => import('../libro/libro.module').then( m => m.LibroPageModule)
      },
      {
        path: 'editar/:id',
        loadChildren: () => import('../editar/editar.module').then( m => m.EditarPageModule)
      },
      {
        path: 'paginas/:tipo/:id',
        loadChildren: () => import('../paginas/paginas.module').then( m => m.PaginasPageModule)
      },
      {
        path: 'ficha/:id',
        loadChildren: () => import('../ficha/ficha.module').then( m => m.FichaPageModule)
      },
      {
        path: 'ajustes',
        loadChildren: () => import('../ajustes/ajustes.module').then( m => m.AjustesPageModule)
      },
      {
        path: 'datos',
        loadChildren: () => import('../datos/datos.module').then( m => m.DatosPageModule)
      },
      {
        path: 'privacidad',
        loadChildren: () => import('../privacidad/privacidad.module').then( m => m.PrivacidadPageModule)
      },
      {
        path: 'contrasena',
        loadChildren: () => import('../contrasena/contrasena.module').then( m => m.ContrasenaPageModule)
      },
      {
        path: 'babybook',
        loadChildren: () => import('../babybook/babybook.module').then( m => m.BabybookPageModule)
      },
      {
        path: 'ayuda',
        loadChildren: () => import('../ayuda/ayuda.module').then( m => m.AyudaPageModule)
      },
      {
        path: 'contacto',
        loadChildren: () => import('../contacto/contacto.module').then( m => m.ContactoPageModule)
      },
      {
        path: 'faq',
        loadChildren: () => import('../faq/faq.module').then( m => m.FaqPageModule)
      },
      {
        path: 'reportar',
        loadChildren: () => import('../reportar/reportar.module').then( m => m.ReportarPageModule)
      },
      {
        path: 'agregar',
        loadChildren: () => import('../agregar/agregar.module').then( m => m.AgregarPageModule)
      },
      {
        path: 'condiciones',
        loadChildren: () => import('../condiciones/condiciones.module').then( m => m.CondicionesPageModule)
      },
      {
        path: 'politicas',
        loadChildren: () => import('../politicas/politicas.module').then( m => m.PoliticasPageModule)
      },
      {
        path: 'normas',
        loadChildren: () => import('../normas/normas.module').then( m => m.NormasPageModule)
      },
      {
        path: 'follows',
        loadChildren: () => import('../follows/follows.module').then( m => m.FollowsPageModule)
      },
      {
        path: 'post/:id',
        loadChildren: () => import('../post/post.module').then( m => m.PostPageModule)
      },
      {
        path: 'hijo/:id',
        loadChildren: () => import('../hijo/hijo.module').then( m => m.HijoPageModule)
      },
      {
        path: '',
        redirectTo: '/tabs/tab1/all',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tab1/all',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
