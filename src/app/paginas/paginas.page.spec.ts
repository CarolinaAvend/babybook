import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PaginasPage } from './paginas.page';

describe('PaginasPage', () => {
  let component: PaginasPage;
  let fixture: ComponentFixture<PaginasPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaginasPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PaginasPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
