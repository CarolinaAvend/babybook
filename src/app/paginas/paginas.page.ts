import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IonSlides } from '@ionic/angular';
import { LibroService } from 'src/app/service/libro.service';
import { formatDate } from '@angular/common';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-paginas',
  templateUrl: './paginas.page.html',
  styleUrls: ['./paginas.page.scss'],
})
export class PaginasPage implements OnInit {
  @ViewChild('slides', { static: false }) slides: IonSlides;

  type: string;
  plantillas = [
    {id: 1, nombre: 'Plantilla Niña', img: "/assets/img/plantilla-niña.png", check: true},
    {id: 2, nombre: 'Plantilla Niño', img: "/assets/img/plantilla-niño.png", check: false}
  ]
  slideOpts = {
    initialSlide: 0,
    speed: 400,
    slidesPerView: 1.1,
    spaceBetween: 20,
    centerSlides:false,
    watchOverflow: true,
};  

  libros = [
    {page: 1, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 2, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 3, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 4, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 5, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 6, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 7, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 8, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 9, img: "https://via.placeholder.com/280x280", comentarios: []},
    {page: 10, img: "https://via.placeholder.com/280x280", comentarios: []},

  ]

  pagecomment = [{
    author_id: 0,
    author_img: '',
    author_name: '',
    create_at: '', 
    id: 0,
    text: '',
  }]
  page = 1;
  id = '';

  nombre = new FormControl('');
  apellido = new FormControl('');
  materno = new FormControl('');
  fecha = new FormControl('');
  hora = new FormControl('');

  hijonombre = ''
  image = ''
  apiurl;

  constructor(private activatedRoute: ActivatedRoute,
    private libro: LibroService) {
    this.type = this.activatedRoute.snapshot.paramMap.get('tipo')
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.libros = [];
    this.page = 1;
    this.type = this.activatedRoute.snapshot.paramMap.get('tipo')
    this.id = this.activatedRoute.snapshot.paramMap.get('id')
    this.apiurl = this.libro.geturl()
    this.getson();
    this.getpaginas();
  }

  selectplantilla(index: number){
    for (let i=0;i<this.plantillas.length;i++){
      if (this.plantillas[i].id == index){
        this.plantillas[i].check = true;
      }else{
        this.plantillas[i].check = false;
      }
    } 
  }


  slideChanged(slides: IonSlides) {
    slides.getActiveIndex().then((index: number) => {
     console.log(index);
     this.page = index + 1;
     this.libro
    });
  }

  getson(){
    let hijo = Number(this.id)
    this.libro.getson(hijo).subscribe((result:any) => {
      console.log(result)
      this.hijonombre = result.data[0].nombre
      this.nombre.setValue(result.data[0].nombre);
      this.apellido.setValue(result.data[0].apellido_paterno);
      this.materno.setValue(result.data[0].apellido_materno);
      this.fecha.setValue(formatDate(new Date(result.data[0].fecha_nacimiento), 'yyyy-MM-dd','es-CL'));
      if (result.data[0].foto){
        this.image = this.apiurl + result.data[0].foto
      }
    }, error =>{
      console.log(error)
    })


  }

  getpaginas(){
    let hijo = Number(this.id)
    let aux = 0;
    this.libro.getpostson(hijo).subscribe((result:any) => {
      console.log(result)
      result[0].data.forEach(element => {
        if (element.pictures.length > 0){
          this.libros.push({page: aux + 1, img: this.apiurl + element.pictures[0].picture, comentarios: element.comments})
        }
      });
    }, error =>{
      console.log(error)
    })

  }

}
