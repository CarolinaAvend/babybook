import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';

@Injectable({
  providedIn: 'root'
})
export class LibroService {

  //apiurl = 'https://apis.ant-desa.cl';
  apiurl = 'http://143.198.71.17';
  options: CameraOptions = {
    targetWidth: 500,
    targetHeight: 500,
    quality: 100,
    //destinationType: this.camera.DestinationType.DATA_URL,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    correctOrientation: true
  }

  constructor(public httpclient: HttpClient,
    private camera: Camera) { }

  geturl(){
    let url = this.apiurl + "/";
    return url
  }

  registro(usuario: any, pass: any, nombre: any, apellido: any, genero: any){
    let url = this.apiurl + "/register";
    let body = {
        username: usuario,
        password: pass,
        name: nombre,
        lastname: apellido,
        gender: genero,
        status: true
    }
    const httpOptions = {headers: new HttpHeaders({})};
    return this.httpclient.post(url,body,httpOptions)
  }

  login(usuario: any, pass: any){
    let url = this.apiurl + "/authenticate";
    let body = {
      username: usuario,
      password: pass,
    };
    const httpOptions = {headers: new HttpHeaders({})};
    return this.httpclient.post(url,body,httpOptions)
  }

  agregarhijo(hijo:any){
    let url = this.apiurl + "/api/add/son";
    let body = {
      nombre: hijo.value.nombre,
      apellido_paterno: hijo.value.apellido,
      apellido_materno: hijo.value.materno,
      genero: Number(hijo.value.sexo),
      fecha_nacimiento: hijo.value.fecha,
      peso: hijo.value.peso,
      talla: hijo.value.talla,
      apgar: hijo.value.apgar,
      lugar_nacimiento: hijo.value.lugar,
      nacionalidad: hijo.value.nacionalidad,
      color_pelo: hijo.value.pelo,
      color_ojos: hijo.value.ojos,
      parecido: hijo.value.parecido,
      direccion: hijo.value.direccion,
      foto: ""
    }
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }


  search(a: any){
    let url = this.apiurl + "/api/search";
    let body = {
      query: a,
    }
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  addpost(titulo: any, descp: any, privacidad: any, sons: any){
    let url = this.apiurl + "/api/add/post";
    let body = {
      title: titulo,
      description: descp,
      privacy:privacidad,
      sons: sons, //'[{"id":1}, {"id": 2}]',
    }
    console.log(body)
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  addUpload(text:any, id:any, image:any){
    let url = this.apiurl + "/api/add/post/upload";
    let body = {
      text: text,
      post_id: id,
      postimage: image,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token"),
      'Access-Control-Allow-Origin': '*'})
    };
    return this.httpclient.post(url,body,httpOptions)
  }


  getsons(){
    let url = this.apiurl + "/api/get/sons";
    let body = {};
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  followers(){
    let url = this.apiurl + "/api/get/followers";
    let body = {};
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  follows(){
    let url = this.apiurl + "/api/get/follows";
    let body = {};
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  addfollow(id: any){
    let url = this.apiurl + "/api/add/follow";
    let body = {
      follow_id: id,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  getpost(){
    let url = this.apiurl + "/api/get/posts";
    let body = {
      
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  getpostson(id: any){
    let url = this.apiurl + "/api/get/son/posts";
    let body = {
      son_id: id,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  getme(){
    let url = this.apiurl + "/api/get/me";
    let body = {
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  editme(usuario:any){
    let url = this.apiurl + "/api/edit/me";
    let body = {
      picture: "",
      username: usuario.username,
      email: usuario.email,
      name: usuario.name,
      lastname: usuario.lastname,
      gender: usuario.gender,
      birth: usuario.birth,
      nationality: usuario.nationality,
      country: usuario.country,
      city: usuario.city,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  changepass(nueva: any){
    let url = this.apiurl + "/api/edit/pass";
    let body = {
      password: nueva,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  
  addcomment(texto: any, id: any){
    let url = this.apiurl + "/api/add/comment";
    let body = {
      text: texto,
      post_id: id
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }


  addlike(id: any){
    let url = this.apiurl + "/api/add/post/like";
    let body = {
      post_id: id,
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  addcommentlike(){
    let url = this.apiurl + "/add/comment/like";
    let body = {
      text: "Hola soy un comentario",
      post_id: 30
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  getson(id: any){
    let url = this.apiurl + "/api/get/son";
    let body = {
      son_id: id
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }

  editson(hijo: any){
    let url = this.apiurl + "/edit/son";
    let body = {
      nombre: hijo.value.nombre,
      apellido_paterno: hijo.value.apellido,
      apellido_materno: hijo.value.materno,
      genero: Number(hijo.value.sexo),
      fecha_nacimiento: hijo.value.fecha,
      peso: hijo.value.peso,
      talla: hijo.value.talla,
      apgar: hijo.value.apgar,
      lugar_nacimiento: hijo.value.lugar,
      nacionalidad: hijo.value.nacionalidad,
      color_pelo: hijo.value.pelo,
      color_ojos: hijo.value.ojos,
      parecido: hijo.value.parecido,
      direccion: hijo.value.direccion,
      foto: ""
    };
    //const httpOptions = {headers: new HttpHeaders({})};
    let httpOptions = {
      headers: new HttpHeaders({'access-token': localStorage.getItem("token")})
    };
    return this.httpclient.post(url,body,httpOptions)
  }


  setpicture(sourceType): Promise<any>{
    return new Promise((resolve, reject) => {
      this.options.sourceType = sourceType;
      this.camera.getPicture(this.options).then((res) => {
        //let base64Image = 'data:image/jpeg;base64,' + res;
        let base64Image = res;
        resolve(base64Image);
      }).catch((err) => {
        reject(err);
      })
    })
  }




}



