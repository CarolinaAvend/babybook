import { Component, OnInit } from '@angular/core';
import { LibroService } from 'src/app/service/libro.service';

@Component({
  selector: 'app-tab5',
  templateUrl: './tab5.page.html',
  styleUrls: ['./tab5.page.scss'],
})
export class Tab5Page implements OnInit {
  //usuario = {id: 30, nombre: 'nombre', apellido: 'administrador', nacionalidad: 'Chilena', edad: 39, img: '/assets/img/ico-user-72x72.svg'}
  usuario = {
    id:0, 
    nombre:'', 
    apellido: '', 
    nacionalidad: '', 
    img: '',
  }
  hijos = [];
  sons = 0;
  seguidores = 0;
  sigues = 0;
  post = 0;
  edad = 0;
  apiurl = '';

  constructor(private libro: LibroService,
    ) {

  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.apiurl = this.libro.geturl()
    this.usuario = {
      id:0, 
      nombre:'', 
      apellido: '', 
      nacionalidad: '', 
      img: '',
    }
    this.hijos = [];
    this.getuser();
    this.gethijos();
  }


  gethijos(){
    this.libro.getsons().subscribe((data: any)=>{
      console.log(data)
      data.data.forEach(element => {
        this.hijos.push({id: element.id, nombre:element.nombre, foto: element.foto})
      });
    }, error =>{
      console.log(error)
    } )      
  }

  getuser(){
    this.libro.getme().subscribe((result:any) => {
      console.log(result)
      this.usuario = {id:result.data[0].id, nombre:result.data[0].name, apellido: result.data[0].lastname, nacionalidad: result.data[0].nationality, img: result.data[0].picture}
      this.seguidores = result.data[0].followers,
      this.sigues = result.data[0].follows,
      this.post = result.data[0].posts_count,
      this.sons = result.data[0].sons_count
      this.calcularedad(result.data[0].birth)
      console.log(this.usuario)
    }, error =>{
      console.log(error)
    })
    console.log(this.usuario)
  }

  calcularedad(fecha: Date){
    if (fecha) {
      var timeDiff = Math.abs(Date.now() - new Date(fecha).getTime());
      this.edad = Math.floor(timeDiff / (1000 * 3600 * 24) / 365.25);
      console.log('Edad:',this.edad)
    }
  }
  

}
